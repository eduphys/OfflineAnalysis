import xarray
#import tables
import h5py
import numpy
import pandas
#import random
import types
#from numba import jit
import dask.dataframe
import dask
import dask.multiprocessing
import DldFlashProcessorCy
import math

import pylab as pl
import DldProcessorV2 as DldProcessor

import sys
sys.path.append("/home/pg2user/PAH")
from camp.pah.beamtimedaqaccess import BeamtimeDaqAccess


"""
  This class reads an existing run and generates a hdf5 file containing the dask data frames.
  It is intended to be used with data generated from August 31, 2017 to September 3, 2017.
"""
defaultPath = '/home/pg2user/copiedFiles/beamtime'
class DldFlashProcessor(DldProcessor.DldProcessor):
    
    """
      The constructor just allows for setting a default center for
      calculating the radii of the electrons.
    """
    def __init__(self):
        super().__init__()
        #self.dldTimeStep = 0.00685 # ns
        self.dldTimeStep = 0.0205761316872428 # ns
        

    """
      Read a run (the run number must be specified). The data is
      expected to be in "/home/pg2user/copiedFiles" (needs to be
      changed for later analysis after the beam time)
    """    
    def readRun(self, runNumber, path=defaultPath, problematicRun = False):
        """
        In some runs, the data storing process was corrupted, e.g in run 18423. Therefore
        we need to access the delayStage differently
        """
        # Import the dataset
        dldPosXName = "/uncategorised/FLASH1_USER2/FLASH.FEL/HEXTOF.DAQ/DLD1:0/dset"
        dldPosYName = "/uncategorised/FLASH1_USER2/FLASH.FEL/HEXTOF.DAQ/DLD1:1/dset"
        dldTimeName = "/uncategorised/FLASH1_USER2/FLASH.FEL/HEXTOF.DAQ/DLD1:3/dset"
        dldMicrobunchIdName = "/uncategorised/FLASH1_USER2/FLASH.FEL/HEXTOF.DAQ/DLD1:2/dset"
        dldAuxName = "/uncategorised/FLASH1_USER2/FLASH.FEL/HEXTOF.DAQ/DLD1:4/dset"
        #delayStageName = "/Experiment/Pump probe laser/laser delay"
        delayStageName = "/Experiment/Pump probe laser/delay line IK220.0/ENC.DELAY"

        bamName = '/Electron Diagnostic/BAM/4DBC3/electron bunch arrival time (low charge)'
        bunchChargeName = '/Electron Diagnostic/Bunch charge/after undulator'
        opticalDiodeName ='/Experiment/PG/SIS8300 100MHz ADC/CH9/pulse energy/TD'
        gmdTunnelName ='/Photon Diagnostic/GMD/Pulse resolved energy/energy tunnel'
        
        #adc1Name = '/Experiment/PG/SIS8300 100MHz ADC/CH6/TD'
        #adc2Name = '/Experiment/PG/SIS8300 100MHz ADC/CH7/TD'
        
        if problematicRun:
            delayStageName = "/Experiment/Pump probe laser/delay line IK220.0/ENC"
            
        
        
        #chunks = 10000000
        
        daqAccess= BeamtimeDaqAccess.create(path)
        
        print('reading DAQ data')
        #~ print("reading dldPosX")
        self.dldPosX, otherStuff = daqAccess.allValuesOfRun(dldPosXName, runNumber)
        #~ print("reading dldPosY") 
        self.dldPosY, otherStuff = daqAccess.allValuesOfRun(dldPosYName, runNumber)
        #~ print("reading dldTime")
        self.dldTime, otherStuff = daqAccess.allValuesOfRun(dldTimeName, runNumber)
        #~ print("reading dldMicrobunchId")
        self.dldMicrobunchId, otherStuff = daqAccess.allValuesOfRun(dldMicrobunchIdName, runNumber)
        #~ print("reading dldAux")
        self.dldAux, otherStuff = daqAccess.allValuesOfRun(dldAuxName, runNumber)
        
        #~ print("reading delayStage")
        self.delaystage, otherStuff = daqAccess.allValuesOfRun(delayStageName, runNumber)
        if problematicRun:
            self.delaystage = self.delaystage[:,1]
        
        #~ print("reading BAM")
        self.bam, otherStuff = daqAccess.allValuesOfRun(bamName, runNumber)
        self.opticalDiode, otherStuff = daqAccess.allValuesOfRun(opticalDiodeName, runNumber)
        #~ print("reading bunchCharge")
        self.bunchCharge, otherStuff = daqAccess.allValuesOfRun(bunchChargeName, runNumber)
        self.gmdTunnel, otherStuff = daqAccess.allValuesOfRun(gmdTunnelName, runNumber)
        electronsToCount = self.dldPosX.copy().flatten()
        electronsToCount =numpy.nan_to_num(electronsToCount)
        electronsToCount = electronsToCount[electronsToCount > 0]
        electronsToCount = electronsToCount[electronsToCount < 10000]
        numOfElectrons = len(electronsToCount)
        print("Number of electrons: " + str(numOfElectrons))
        print("Creating data frame: Please wait...")
        self.createDataframePerElectron()
        self.createDataframePerMicrobunch()
        print('dataframe created')



    """ 
      Create a data frame from the read arrays (either from the test file or the run number)
    """
    def createDataframePerElectron(self):
        chunks = 10000000
        daX = dask.array.from_array(self.dldPosX.flatten(), chunks=(chunks))
        daY = dask.array.from_array(self.dldPosY.flatten(), chunks=(chunks))
       
        dldDetectorId = self.dldTime%2
        
        self.dldTime=self.dldTime*self.dldTimeStep
        daTime = dask.array.from_array(self.dldTime.flatten(), chunks=(chunks))
        
        numOfMacrobunches = self.bam.shape[0]
        
        
        
        
        
        # convert the bam data to electron format
        bamArray = DldFlashProcessorCy.assignToMircobunch(self.dldMicrobunchId.astype(numpy.float64), self.bam.astype(numpy.float64))
        
        # convert the delay stage position to the electron format
        delaystageArray = numpy.zeros_like(self.dldMicrobunchId)
        #for macro in range(0, numOfMacrobunches):
        delaystageArray[:, :] =  (self.delaystage[:,0])[:,None]
       
        daDelaystage = dask.array.from_array(delaystageArray.flatten(), chunks=(chunks))
        
        daBam = dask.array.from_array(bamArray.flatten(), chunks=(chunks))
        
        daMicrobunchId = dask.array.from_array(self.dldMicrobunchId.flatten(), chunks=(chunks))
        
        daDetectorId = dask.array.from_array(dldDetectorId.flatten(), chunks=(chunks))
        
        bunchChargeArray = DldFlashProcessorCy.assignToMircobunch(self.dldMicrobunchId.astype(numpy.float64), self.bunchCharge.astype(numpy.float64))
        daBunchCharge = dask.array.from_array(bunchChargeArray.flatten(), chunks=(chunks))
        
        opticalDiodeArray = DldFlashProcessorCy.assignToMircobunch(self.dldMicrobunchId.astype(numpy.float64), self.opticalDiode.astype(numpy.float64))
        daOpticalDiode = dask.array.from_array(opticalDiodeArray.flatten(), chunks=(chunks))
        
        gmdTunnelArray = DldFlashProcessorCy.assignToMircobunch(self.dldMicrobunchId.astype(numpy.float64), self.gmdTunnel.astype(numpy.float64))
        daGmdTunnel = dask.array.from_array(gmdTunnelArray.flatten(), chunks=(chunks))
        
       
        # the Aux channel: aux0:
        dldAux0 = self.dldAux[:,0]
        self.aux0 = numpy.ones(self.dldTime.shape)*dldAux0[:,None]
        daAux0 = dask.array.from_array(self.aux0.flatten(), chunks=(chunks))
        # the Aux channel: aux1:
        dldAux1 = self.dldAux[:,1]
        self.aux1 = numpy.ones(self.dldTime.shape)*dldAux1[:,None]
        daAux1 = dask.array.from_array(self.aux1.flatten(), chunks=(chunks))
        
        da = dask.array.stack([daX, daY, daTime, daDelaystage, daBam, daMicrobunchId, daAux0, daAux1, daDetectorId, daBunchCharge, daOpticalDiode, daGmdTunnel])
        
        # create the data frame:
        self.dd = dask.dataframe.from_array(da.T, 
                                                    columns=('posX','posY', 'dldTime', 'delayStageTime', 'bam', 'microbunchId', 'aux0', 'aux1', 'dldDetectorId', 'bunchCharge', 'opticalDiode', 'gmdTunnel'))
        self.dd = self.dd[self.dd['microbunchId'] > 0]
        
        
        
    def createDataframePerMicrobunch(self):
        chunks = 10000000
        
        numOfMacrobunches = self.bam.shape[0]
        
        # convert the delay stage position to the electron format
        delaystageArray = numpy.zeros_like(self.bam)
        delaystageArray[:, :] = (self.delaystage[:,0])[:, None]
       
        daDelaystage = dask.array.from_array(delaystageArray.flatten(), chunks=(chunks))
        
        daBam = dask.array.from_array(self.bam.flatten(), chunks=(chunks))
        numOfMicrobunches = self.bam.shape[1]
       
        # the Aux channel: aux0:
        dldAux0 = self.dldAux[:,0]
        aux0 = numpy.ones(self.bam.shape)*dldAux0[:,None]
        daAux0 = dask.array.from_array(aux0.flatten(), chunks=(chunks))
        # the Aux channel: aux1:
        dldAux1 = self.dldAux[:,1]
        aux1 = numpy.ones(self.bam.shape)*dldAux1[:,None]
        daAux1 = dask.array.from_array(aux1.flatten(), chunks=(chunks))
        
       
        daBunchCharge = dask.array.from_array(self.bunchCharge[:,0:numOfMicrobunches].flatten(), chunks=(chunks))
        
        lengthToPad = numOfMicrobunches - self.opticalDiode.shape[1]
        paddedOpticalDiode = numpy.pad(self.opticalDiode, ((0,0),(0,lengthToPad)), 'constant', constant_values=(0,0))
        daOpticalDiode = dask.array.from_array(paddedOpticalDiode.flatten(), chunks=(chunks))
        
        da = dask.array.stack([daDelaystage, daBam, daAux0, daAux1, daBunchCharge, daOpticalDiode])
        
        # create the data frame:
        self.ddMicrobunches = dask.dataframe.from_array(da.T, 
                                                    columns=('delayStageTime', 'bam', 'aux0', 'aux1', 'bunchCharge', 'opticalDiode'))
                                                    
            
    def storeDataframes(self, fileName):
        dask.dataframe.to_hdf(self.dd, fileName, '/electrons')
        dask.dataframe.to_hdf(self.ddMicrobunches, fileName, '/microbunches')
        
        
    def storeDataframesParquet(self, fileName):
        self.dd.to_parquet(fileName + "_el", compression="UNCOMPRESSED")
        self.ddMicrobunches.to_parquet(fileName + "_mb", compression="UNCOMPRESSED")
