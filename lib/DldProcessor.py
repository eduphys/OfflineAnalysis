import xarray
#import tables
import h5py
import numpy
import pandas
#import random
import types
#from numba import jit
import dask.dataframe
import dask
import dask.multiprocessing
import DldFlashProcessorCy
import math

import pylab as pl


import sys



"""
  This class simplifies the analysis of data files recorded during the
  beamtime of August 2017. It converts the electrons from the DLD into
  a clean table and uses DASK for binning and further analysis.
"""
class DldProcessor():
    
    """
      The constructor just allows for setting a default center for
      calculating the radii of the electrons.
    """
    def __init__(self):
        self.dldTimeStep = 0.0205761316872428 # ns
        self.deleteBinners()
        
    def readDataframes(self, fileName):
        self.dd = dask.dataframe.read_hdf(fileName, '/electrons', mode='r', chunksize=10000000)
        self.ddMicrobunches = dask.dataframe.read_hdf(fileName, '/microbunches', mode='r', chunksize=10000000)
        #self.postProcess()
        
    def readDataframesParquet(self, fileName):
        self.dd = dask.dataframe.read_parquet(fileName + "_el", engine="arrow")
        self.ddMicrobunches = dask.dataframe.read_parquet(fileName + "_mb", engine="arrow")
        
        
        
    def postProcess(self, centerX=256, centerY=256):
        self.dd['pumpProbeTime'] = self.dd['delayStageTime'] - self.dd['bam']*1e-3*0.0
        self.ddMicrobunches['pumpProbeTime'] = self.ddMicrobunches['delayStageTime'] - self.ddMicrobunches['bam']*1e-3
        def radius(df):
            return numpy.sqrt(numpy.square(df.posX-centerX) + numpy.square(df.posY-centerY))
        self.dd['posR'] = self.dd.map_partitions(radius)
        
    
    """
    Store the binned data in a hdf5 file
    
    Attributes
    binneddata: binned data with binnes in dldTime, posX, and posY (and if to be normalized, binned in detectors)
    filename: name of the file
    normalize: Normalized data for both detector, so it should be a 3d array (posX, posY,detectorID) 
    """
    def save2hdf5(self,binnedData, path = '/home/pg2user/OfflineAnalysis/',  filename='default.hdf5', normalizedData=None, overwrite=False):
        
        # normalization given, for example take it from run 18440
        """
        processor.readRun(18440)
        processor.addBinning('posX', 500, 1000, 2)
        processor.addBinning('posY', 500, 1000, 2)
        processor.addBinning('dldDetectorId', -1, 2, 1)
        norm = processor.computeBinnedData()
        norm = np.nan_to_num(norm)
        norm[norm<10]=1 # 10 or smaller seems to be outside of detector
        
        norm[:,:,0][norm[:,:,0] >= 10]/=norm[:,:,0][norm[:,:,0] >= 10].mean()
        norm[:,:,1][norm[:,:,1] >= 10]/=norm[:,:,1][norm[:,:,1] >= 10].mean()
        norm[norm<0.05]=0.1
        """
        if normalizedData is not None:
            if binnedData.ndim != 4:
                raise Exception('Wrong dimension')
            data2hdf5 = numpy.zeros_like(binnedData[:,:,:,0])    
            
            
            # normalize for all time binns
            for i in range(binnedData.shape[0]):
                # normalize for both detectors (0 and 1)
                
                data2hdf5[i,:,:] = binnedData[i,:,:,0].transpose()/normalizedData[:,:,0].transpose()
                data2hdf5[i,:,:] += binnedData[i,:,:,1].transpose()/normalizedData[:,:,1].transpose()
        else:
            # detector binned? -> sum together
            if binnedData.ndim == 4:
                data2hdf5 = binnedData.sum(axis=3).transpose((0,2,1))
            else:
                if binnedData.ndim != 3:
                    raise Exception('Wrong dimension')
                #print(binnedData.transpose((1,2).shape)
                data2hdf5 = binnedData.transpose((0,2,1))
        
        # create file and save data
        mode = "w-" # fail if file exists
        if overwrite:
            mode = "w"
        
        f = h5py.File(path+filename, mode)
        dset = f.create_dataset("experiment/xyt_data", data2hdf5.shape, dtype='float64')
        
        dset[...] = data2hdf5
        f.close()
        print("Created file "+filename)
    
    """
     Add a binning:
      name: name of the column to bin to
      start, end, stepSize: interval used for binning
      If the name is 'pumpProbeTime': sets self.delaystageHistogram for normalization.
    """
    def addBinning(self, name, start, end, stepSize):
        bins = numpy.arange(start, end, stepSize)
        # do the binning:
        binner = self.dd[name].map_partitions(pandas.cut, bins)
        self.binnerList.append(binner)
        if (name == 'pumpProbeTime'):
            #self.delaystageHistogram = numpy.histogram(self.delaystage[numpy.isfinite(self.delaystage)], bins)[0]
            delaystageHistBinner = self.ddMicrobunches['pumpProbeTime'].map_partitions(pandas.cut, bins)
            delaystageHistGrouped = self.ddMicrobunches.groupby([delaystageHistBinner])
            self.delaystageHistogram = delaystageHistGrouped.count().compute()['bam'].to_xarray().values.astype(numpy.float64)
        
    
    """
     make an empty binner list
    """
    def deleteBinners(self):
        self.binnerList = []
        
    
    
    """
     use the binner list to bin the data. It returns a numpy array.
    """
    def computeBinnedData(self):
        # group the data in dld time:
        grouped = self.dd.groupby(self.binnerList)
        # count the electrons
        self.counted = grouped.count().compute()['microbunchId']
        return self.counted.to_xarray().values.astype(numpy.float64)
    
   
