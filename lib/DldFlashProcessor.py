import xarray
#import tables
import h5py
import numpy
import pandas
#import random
import types
#from numba import jit
import dask.dataframe
import dask
import dask.multiprocessing
import DldFlashProcessorCy
import math

import pylab as pl


import sys
sys.path.append("/home/pg2user/PAH")
from camp.pah.beamtimedaqaccess import BeamtimeDaqAccess


"""
  This class simplifies the analysis of data files recorded during the
  beamtime of August 2017. It converts the electrons from the DLD into
  a clean table and uses DASK for binning and further analysis.
"""
class DldFlashProcessor():
    
    """
      The constructor just allows for setting a default center for
      calculating the radii of the electrons.
    """
    def __init__(self, centerX = 2000, centerY = 2000):
        self.centerX = centerX
        self.centerY = centerY
        self.isDD = False
        self.dldTimeStep = 0.0205761316872428 # ns
        

    """
      Read a run (the run number must be specified). The data is
      expected to be in "/home/pg2user/copiedFiles" (needs to be
      changed for later analysis after the beam time)
    """    
    def readRun(self, runNumber, problematicRun = False):
        """
        In some runs, the data storing process was corrupted, e.g in run 18423. Therefore
        we need to access the delayStage differently
        """
        # Import the dataset
        dldPosXName = "/uncategorised/FLASH1_USER2/FLASH.FEL/HEXTOF.DAQ/DLD1:0/dset"
        dldPosYName = "/uncategorised/FLASH1_USER2/FLASH.FEL/HEXTOF.DAQ/DLD1:1/dset"
        dldTimeName = "/uncategorised/FLASH1_USER2/FLASH.FEL/HEXTOF.DAQ/DLD1:3/dset"
        dldMicrobunchIdName = "/uncategorised/FLASH1_USER2/FLASH.FEL/HEXTOF.DAQ/DLD1:2/dset"
        dldAuxName = "/uncategorised/FLASH1_USER2/FLASH.FEL/HEXTOF.DAQ/DLD1:4/dset"
        #delayStageName = "/Experiment/Pump probe laser/laser delay"
        delayStageName = "/Experiment/Pump probe laser/delay line IK220.0/ENC.DELAY"

        bamName = '/Electron Diagnostic/BAM/4DBC3/electron bunch arrival time (low charge)'
        bunchChargeName = '/Electron Diagnostic/Bunch charge/after undulator'
        opticalDiodeName ='/Experiment/PG/SIS8300 100MHz ADC/CH9/pulse energy/TD'
        #adc1Name = '/Experiment/PG/SIS8300 100MHz ADC/CH6/TD'
        #adc2Name = '/Experiment/PG/SIS8300 100MHz ADC/CH7/TD'
        
        if problematicRun:
            delayStageName = "/Experiment/Pump probe laser/delay line IK220.0/ENC"
            
        
        
        chunks = 10000000
        
        #daqAccess= BeamtimeDaqAccess.create("/gpfs/pg2/commissioning/raw/hdf/online/fl1user2/")
        #daqAccess= BeamtimeDaqAccess.create("/home/pg2user/copiedFiles")
        daqAccess= BeamtimeDaqAccess.create("/home/pg2user/copiedFiles/beamtime")
        
        
        #~ print("reading dldPosX")
        self.dldPosX, otherStuff = daqAccess.allValuesOfRun(dldPosXName, runNumber)
        #~ print("reading dldPosY") 
        self.dldPosY, otherStuff = daqAccess.allValuesOfRun(dldPosYName, runNumber)
        #~ print("reading dldTime")
        self.dldTime, otherStuff = daqAccess.allValuesOfRun(dldTimeName, runNumber)
        #~ print("reading dldMicrobunchId")
        self.dldMicrobunchId, otherStuff = daqAccess.allValuesOfRun(dldMicrobunchIdName, runNumber)
        #~ print("reading dldAux")
        self.dldAux, otherStuff = daqAccess.allValuesOfRun(dldAuxName, runNumber)
        
        #~ print("reading delayStage")
        self.delaystage, otherStuff = daqAccess.allValuesOfRun(delayStageName, runNumber)
        if problematicRun:
            self.delaystage = self.delaystage[:,1]
        
        #~ print("reading BAM")
        self.bam, otherStuff = daqAccess.allValuesOfRun(bamName, runNumber)
        self.opticalDiode, otherStuff = daqAccess.allValuesOfRun(opticalDiodeName, runNumber)
        #~ print("reading bunchCharge")
        self.bunchCharge, otherStuff = daqAccess.allValuesOfRun(bunchChargeName, runNumber)
        self.createDataframe(withAux=True)

    """
     read the hdf5 file (in Zurich)
     
    """
    def readHdfFile(self, fileName):
        print("Reading hdf5 file. Please wait...")
        # Read the hdf5 file
        h5File = h5py.File(fileName, "r")
       
        self.delaystage = h5File["DLD/delayStage"].value 
        self.dldPosX = h5File["/DLD/dldPosX"].value
        self.dldPosY = h5File["/DLD/dldPosY"].value
        self.dldTime = h5File["/DLD/dldTime"].value
        self.dldMicrobunchId = h5File["/DLD/dldMicrobunchId"].value
        self.bam = h5File["/DLD/bam"].value
        self.createDataframe()



    """ 
      Create a data frame from the read arrays (either from the test file or the run number)
    """
    def createDataframe(self, withAux=False):
        print("Creating data frame: Please wait...")
        chunks = 10000000
        daX = dask.array.from_array(self.dldPosX.flatten(), chunks=(chunks))
        daY = dask.array.from_array(self.dldPosY.flatten(), chunks=(chunks))
        # calculate a new array containing the radii
        dldRadii = numpy.sqrt(numpy.square(self.dldPosX-self.centerX) + numpy.square(self.dldPosY-self.centerY))
        daRadii = dask.array.from_array(dldRadii.flatten(), chunks=(chunks))
        
        
        self.dldDetectorId = self.dldTime%2
        
        self.dldTime=self.dldTime*self.dldTimeStep
        daTime = dask.array.from_array(self.dldTime.flatten(), chunks=(chunks))
        
        numOfMacrobunches = self.bam.shape[0]
        
        ######### TO AVIOD TDC READOUT PROBLEM WITH THE MICROBUNCH ID##########
        # we need to low pass filter the BAM data. We do a running average using summed-up arrays
        # (needed due to problems with the TDC, as it misses 4 bits...)
        bamFiltered = self.bam.copy()
        ones = numpy.ones_like(self.bam, dtype=numpy.float)
        # counts the number of additions for the normalization.
        normalizer = ones.copy()
        for i in range(1, 5):
            bamFiltered[:, i:] = bamFiltered[:, i:] + self.bam[:, :-i]
            normalizer[:, i:] = normalizer[:, i:] + ones[:, :-i]
        self.bamFiltered = bamFiltered / normalizer
        # now we multiply the microbunch ID by 4 (needed due to problems with the TDC, as it misses 4 bits...)
        self.dldMicrobunchId = self.dldMicrobunchId * 4
        #####################################################################
        
        
        
        # convert the bam data to electron format
        timeArr = DldFlashProcessorCy.assignToMircobunch(self.dldMicrobunchId.astype(numpy.float64), self.bamFiltered.astype(numpy.float64))
        #mba = self.dldMicrobunchId.astype(int)
        

        # and add the delay position to it (not a very nice solution, I know...)
        # careful: The BAM is in fs, whereas the delay stage is in ps!
        for macro in range(0, numOfMacrobunches):
            # the BAM is supposed to be in fs, but is actually in ps (oitherwise it does not make sense)
            timeArr[macro, :] = -timeArr[macro, :] + self.delaystage[macro]
       
        daPumpProbe = dask.array.from_array(timeArr.flatten(), chunks=(chunks))
        daMicrobunchId = dask.array.from_array(self.dldMicrobunchId.flatten(), chunks=(chunks))
        
        daDetectorId = dask.array.from_array(self.dldDetectorId.flatten(), chunks=(chunks))
        
        
        if (withAux):
            # the Aux channel: aux0:
            dldAux0 = self.dldAux[:,0]
            self.aux0 = numpy.ones(self.dldTime.shape)*dldAux0[:,None]
            daAux0 = dask.array.from_array(self.aux0.flatten(), chunks=(chunks))
        
            # the Aux channel: aux0:
            dldAux1 = self.dldAux[:,1]
            self.aux1 = numpy.ones(self.dldTime.shape)*dldAux1[:,None]
            daAux1 = dask.array.from_array(self.aux1.flatten(), chunks=(chunks))
        
            da = dask.array.stack([daX, daY, daRadii, daTime, daPumpProbe, daMicrobunchId, daAux0, daAux1, daDetectorId])
        
        
            # create the data frame:
            self.dd = dask.dataframe.from_array(da.T, 
                                                    columns=('posX','posY', 'posRadius', 'dldTime', 'pumpProbeTime', 'microbunchId', 'aux0', 'aux1', 'dldDetectorId'))
        else:
            da = dask.array.stack([daX, daY, daRadii, daTime, daPumpProbe, daMicrobunchId, daDetectorId])
            # create the data frame:
            self.dd = dask.dataframe.from_array(da.T, 
                                                    columns=('posX','posY', 'posRadius', 'dldTime', 'pumpProbeTime', 'microbunchId', 'dldDetectorId'))
                                            
        # filter wrong events
        self.dd = self.dd[self.dd['posX'] > 0]
        self.dd = self.dd[self.dd['posY'] > 0]
        
        # filter "nan"- events
        self.dd = self.dd[self.dd['dldTime'] != math.nan]
        self.dd = self.dd[self.dd['pumpProbeTime'] != math.nan]
        
        
        # empty list for binners
        self.binnerList = []
        
        # Histogram of the delay stage position (filled by addBinning if we call it with 'pumpProbeTime') 
        self.delaystageHistogram = []
        
        # Set the DD created flag true
        self.isDD = True
    
    """
    Store the binned data in a hdf5 file
    
    Attributes
    binneddata: binned data with binnes in dldTime, posX, and posY (and if to be normalized, binned in detectors)
    filename: name of the file
    normalize: Normalized data for both detector, so it should be a 3d array (posX, posY,detectorID) 
    """
    def save2hdf5(self,binnedData, path = '/home/pg2user/OfflineAnalysis/',  filename='default.hdf5', normalizedData=None, overwrite=False):
        
        # normalization given, for example take it from run 18440
        """
        processor.readRun(18440)
        processor.addBinning('posX', 500, 1000, 2)
        processor.addBinning('posY', 500, 1000, 2)
        processor.addBinning('dldDetectorId', -1, 2, 1)
        norm = processor.computeBinnedData()
        norm = np.nan_to_num(norm)
        norm[norm<10]=1 # 10 or smaller seems to be outside of detector
        
        norm[:,:,0][norm[:,:,0] >= 10]/=norm[:,:,0][norm[:,:,0] >= 10].mean()
        norm[:,:,1][norm[:,:,1] >= 10]/=norm[:,:,1][norm[:,:,1] >= 10].mean()
        norm[norm<0.05]=0.1
        """
        if normalizedData is not None:
            if binnedData.ndim != 4:
                raise Exception('Wrong dimension')
            data2hdf5 = numpy.zeros_like(binnedData[:,:,:,0])    
            
            
            # normalize for all time binns
            for i in range(binnedData.shape[0]):
                # normalize for both detectors (0 and 1)
                
                data2hdf5[i,:,:] = binnedData[i,:,:,0].transpose()/normalizedData[:,:,0].transpose()
                data2hdf5[i,:,:] += binnedData[i,:,:,1].transpose()/normalizedData[:,:,1].transpose()
        else:
            # detector binned? -> sum together
            if binnedData.ndim == 4:
                data2hdf5 = binnedData.sum(axis=3).transpose((0,2,1))
            else:
                if binnedData.ndim != 3:
                    raise Exception('Wrong dimension')
                #print(binnedData.transpose((1,2).shape)
                data2hdf5 = binnedData.transpose((0,2,1))
        
        # create file and save data
        mode = "w-" # fail if file exists
        if overwrite:
            mode = "w"
        
        f = h5py.File(path+filename, mode)
        dset = f.create_dataset("experiment/xyt_data", data2hdf5.shape, dtype='float64')
        
        dset[...] = data2hdf5
        f.close()
        print("Created file "+filename)
    
    """
     Add a binning:
      name: name of the column to bin to
      start, end, stepSize: interval used for binning
      If the name is 'pumpProbeTime': sets self.delaystageHistogram for normalization.
    """
    def addBinning(self, name, start, end, stepSize):
        if (not self.isDD):
            createDataframe()
        bins = numpy.arange(start, end, stepSize)
        # do the binning:
        binner = self.dd[name].map_partitions(pandas.cut, bins)
        self.binnerList.append(binner)
        if (name == 'pumpProbeTime'):
            self.delaystageHistogram = numpy.histogram(self.delaystage[numpy.isfinite(self.delaystage)], bins)[0]
        
    
    """
     make an empty binner list
    """
    def deleteBinners(self):
        if (not self.isDD):
            createDataframe()
        self.binnerList = []
        
    
    
    """
     use the binner list to bin the data. It returns a numpy array.
    """
    def computeBinnedData(self):
        if (not self.isDD):
            createDataframe()
        # group the data in dld time:
        grouped = self.dd.groupby(self.binnerList)
        # count the electrons
        self.counted = grouped.count().compute()['microbunchId']
        return self.counted.to_xarray().values
    
    """
    Fast check with plots for effects of pump-probe setup
    """
    def fastpumpProbePlotCheck(self, runNumber, timeFrom, timeTo, timeStep, delayFrom, delayTo, delayStep, problematicRun=False):
        
        self.readRun(runNumber, problematicRun)
        self.addBinning('dldTime', timeFrom, timeTo, timeStep)
        
        self.addBinning('pumpProbeTime', delayFrom, delayTo, delayStep)
        result = self.computeBinnedData()
        result = numpy.nan_to_num(result)
        
        # dived through histogramm of delay stages, because some positions
        # were approached more often -> result1
        result1 = numpy.zeros_like(result)
        for i in range(result.shape[0]):
            result1[i,:] = result[i,:] / self.delaystageHistogram
            
        mean = result1.mean(axis=1)
        
        # range must be a step lower than the binning range
        dldtime = numpy.arange(timeFrom,timeTo-timeStep,timeStep)
        pptime = numpy.arange(delayFrom,delayTo-delayStep,delayStep)

        
        pl.figure()
        pl.plot(dldtime,mean)
        pl.grid()
        pl.xlabel('dldTime (ns)')
        pl.ylabel('Counts')
        
        pl.figure()
        pl.plot(pptime, result1[:,:].sum(axis=0))
        pl.xlabel('pumpProbedelay (ps)')
        pl.ylabel('energy histogram')
        pl.grid()
        
        # compute difference
        diff = numpy.zeros_like(result1)
        for i in range(diff.shape[1]):
            diff[:,i]=result1[:,i]-mean
            #diff[:,i]=result1[:,i]

        # give plots of result1 and diff
        pl.figure(figsize = (6,4))
        pl.imshow(result1,aspect = 'auto',extent = (pptime[0],pptime[-1],dldtime[-1],dldtime[0]))
        pl.colorbar()
        pl.ylabel('dldTime (ns)')
        pl.xlabel('pumpProbedelay (ps)')
        pl.figure(figsize = (6,4))
        pl.imshow(diff,aspect = 'auto',extent = (pptime[0],pptime[-1],dldtime[-1],dldtime[0]),clim = (-1,1))
        pl.colorbar()
        pl.ylabel('dldTime (ns)')
        pl.xlabel('pumpProbedelay (ps)')
        
        # to be able to compute it furhtermore, return values
        return (result, result1, diff)
