
"""Functions for calculation of pulse energy and pulse energy density of optical laser.
Calibration values taken from Pump beam energz converter 800 400.xls
Units are uJ for energy, um for beam diameter, uJ/cm^2 for energy density (and arb. for diode signal)"""

import numpy as np

def PulseEnergy400(Diode):
	return 0.86*(Diode*0.0008010439+0.0352573)

def PulseEnergy800(Diode):
	return 0.86*(Diode*0.0009484577+0.1576)

def EnergyDensity400(Diode, Diameter = 600):
	return PulseEnergy400(Diode) / (np.pi*np.square((Diameter*0.0001)/2))

def EnergyDensity800(Diode, Diameter = 600):
	return PulseEnergy800(Diode) / (np.pi*np.square((Diameter*0.0001)/2))
